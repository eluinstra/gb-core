/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.job;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.CompressionLevel;
import net.lingala.zip4j.model.enums.CompressionMethod;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content.Transport.Part;
import nl.ordina.digikoppeling.gb.FileSplitter;
import nl.ordina.digikoppeling.gb.Utils;
import nl.ordina.digikoppeling.gb.dao.DAOException;
import nl.ordina.digikoppeling.gb.dao.GBDAO;
import nl.ordina.digikoppeling.gb.model.send.Compression;
import nl.ordina.digikoppeling.gb.model.send.Status;
import nl.ordina.digikoppeling.gb.model.send.Transaction;

public class PackageTransactionsJob implements Job
{
	protected transient Log logger = LogFactory.getLog(getClass());
	private final static long MIN_SIZE = 65536;
	private GBDAO gbDAO;
	private String fileShare;
	private String workingDir;

	public PackageTransactionsJob(GBDAO gbDAO, String fileShare, String workingDir)
	{
		this.gbDAO = gbDAO;
		this.fileShare = Utils.correctUri(fileShare);
		this.workingDir = Utils.correctUri(workingDir);
	}

	@Override
	public void execute()
	{
		try
		{
			logger.debug(getClass().getName() + " started");
			List<String> transactionIds = gbDAO.getTransactionIds(Status.PENDING);
			logger.debug(transactionIds.size() + " transactions found");
			for (String transactionId : transactionIds)
				packageTransaction(transactionId);
		}
		catch (DAOException | IOException e)
		{
			logger.error("",e);
		}
		finally
		{
			logger.debug(getClass().getName() + " ended");
		}
	}

	private void packageTransaction(String transactionId) throws ZipException, IOException
	{
		try
		{
			logger.info("Packaging transaction " + transactionId);
			Transaction transaction = gbDAO.getTransaction(transactionId);
			File file = getFile(transaction.getFilename());
			List<String> splitFiles = new ArrayList<String>();
			if (Compression.ZIP4J.equals(transaction.getCompression()))
			{
				ZipFile zipFile = createZipFile(transaction,file);
				splitFiles = getSplitZipFiles(zipFile);
			}
			else
			{
				if (transaction.getMaxFileSize() == 0 || file.length() < transaction.getMaxFileSize())
					copyFile(transaction,file);
				else
					splitFiles = createSplitFiles(transaction,file);
			}
			DataReferenceRequest response = new DataReferenceRequestBuilder(transaction,file,splitFiles).build();
			List<Part> parts = response.getContent().getTransport().getPart().size() == 0 ? createPartList(response) : response.getContent().getTransport().getPart();
			gbDAO.updateTransaction(transactionId,response,parts,Status.READY_TO_SEND);
		}
		catch (IOException e)
		{
			logger.info("Transaction " + transactionId + " " + Status.FAILED);
			gbDAO.updateTransaction(transactionId,Status.FAILED,e.getMessage());
			throw e;
		}
	}

	private void copyFile(Transaction transaction, File file) throws IOException, FileNotFoundException
	{
		try (FileInputStream fis = new FileInputStream(file))
		{
			String fileName = file.getName();
			File newFile = new File(workingDir + transaction.getId(),fileName);
			try (FileOutputStream fos = new FileOutputStream(newFile))
			{
				IOUtils.copyLarge(fis,fos);
			}
		}
	}

	private List<Part> createPartList(DataReferenceRequest response)
	{
		List<Part> result = new ArrayList<Part>();
		result.add(createPart(response));
		return result;
	}

	private Part createPart(DataReferenceRequest response)
	{
		Part result = new Part();
		Content content = response.getContent();
		result.setFilename(content.getFilename());
		result.setSize(content.getSize());
		result.setChecksum(content.getChecksum());
		result.setLocation(content.getTransport().getLocation());
		return result;
	}

	private List<String> createSplitFiles(Transaction transaction, File file) throws IOException
	{
		FileSplitter fileSplitter = new FileSplitter(workingDir + transaction.getId(),file,transaction.getMaxFileSize());
		fileSplitter.split();
		return fileSplitter.getSplitFiles();
	}

  private List<String> getSplitZipFiles(ZipFile zipFile) throws ZipException
	{
		List<String> result = new ArrayList<String>();
		for (Object filename : zipFile.getSplitZipFiles())
			result.add(Utils.fixZipFileExension((String)filename));
		return result;
	}

	private ZipFile createZipFile(Transaction transaction, File file) throws ZipException, IOException
	{
		String directory = workingDir + transaction.getId();
		ZipFile zipFile = new ZipFile(directory + "/" + transaction.getFilename() + ".zip");
		zipFile.createSplitZipFile(Arrays.asList(file),createZipParameters(),transaction.getMaxFileSize() != 0,transaction.getMaxFileSize() < MIN_SIZE ? MIN_SIZE : transaction.getMaxFileSize());
		return zipFile;
	}

	private File getFile(String filename) throws IOException
	{
		String f = fileShare + filename;
		File result = new File(f);
		if (!result.exists())
			throw new IOException(f + " not found!");
		return result;
	}

	private ZipParameters createZipParameters()
	{
		ZipParameters parameters = new ZipParameters();
		parameters.setCompressionMethod(CompressionMethod.DEFLATE);
		parameters.setCompressionLevel(CompressionLevel.NORMAL);
		return parameters;
	}

	public void setWorkingDir(String workingDir)
	{
		if (!workingDir.endsWith("/") || workingDir.endsWith("\\"))
			workingDir += "/";
		this.workingDir = workingDir;
	}

	public void setGbDAO(GBDAO gbDAO)
	{
		this.gbDAO = gbDAO;
	}

}
