/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.job;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content.Transport.Part;
import nl.ordina.digikoppeling.gb.Utils;
import nl.ordina.digikoppeling.gb.client.HttpClient;
import nl.ordina.digikoppeling.gb.dao.DAOException;
import nl.ordina.digikoppeling.gb.dao.GBDAO;
import nl.ordina.digikoppeling.gb.model.send.Status;
import nl.ordina.digikoppeling.gb.model.send.Transaction;

public class SendTransactionsJob implements Job
{
	protected transient Log logger = LogFactory.getLog(getClass());
	private GBDAO gbDAO;
	private String workingDir;
	private HttpClient httpClient;

	public SendTransactionsJob(GBDAO gbDAO, String workingDir, HttpClient httpClient)
	{
		this.gbDAO = gbDAO;
		this.workingDir = Utils.correctUri(workingDir);
		this.httpClient = httpClient;
	}

	@Override
	public void execute()
	{
		try
		{
			logger.debug(getClass().getName() + " started");
			List<String> transactionIds = gbDAO.getTransactionIds(Status.READY_TO_SEND);
			logger.debug(transactionIds.size() + " transactions found");
			for (String transactionId : transactionIds)
				sendTransaction(transactionId);
		}
		catch (DAOException | IOException e)
		{
			logger.error("",e);
		}
		finally
		{
			logger.debug(getClass().getName() + " ended");
		}
	}

	private void sendTransaction(String transactionId) throws IOException
	{
		try
		{
			logger.info("Sending transaction " + transactionId);
			Transaction transaction = gbDAO.getTransaction(transactionId);
			List<String> filenames = gbDAO.getFilenames(transactionId,Status.READY_TO_SEND);
			for (String filename : filenames)
				sendFile(transaction,filename);
			logger.info("Transaction " + transactionId + " " + Status.SUCCEEDED);
			gbDAO.updateTransaction(transactionId,Status.SUCCEEDED);
		}
		catch (IOException e)
		{
			logger.info("Transaction " + transactionId + " " + Status.FAILED);
			gbDAO.updateTransaction(transactionId,Status.FAILED,e.getMessage());
			throw e;
		}
	}

	private void sendFile(Transaction transaction, String filename) throws IOException
	{
		try
		{
			Part part = getPart(transaction,filename);
			String filePath = getFilePath(transaction,filename);
			logger.info("Sending file " + filePath + " to " + part.getLocation().getReceiverUrl().getValue());
			httpClient.sendMessage(filePath,part.getLocation().getReceiverUrl().getValue());
			gbDAO.updateFile(transaction.getId(),filename,Status.SUCCEEDED);
		}
		catch (IOException e)
		{
			gbDAO.updateFile(transaction.getId(),filename,Status.FAILED);
			throw e;
		}
	}

	private String getFilePath(Transaction transaction, String filename)
	{
		return workingDir + transaction.getId() + "/" + filename;
	}

	private Part getPart(Transaction transaction, String filename) throws IOException
	{
		if (filename.equals(transaction.getDataReference().getContent().getFilename()))
			return createPart(transaction.getDataReference().getContent());
		for (Part part : transaction.getDataReference().getContent().getTransport().getPart())
			if (filename.equals(part.getFilename()))
				return part;
		throw new IOException("Filename " + filename + " not found in transaction " + transaction.getId());
	}

	private Part createPart(Content content)
	{
		Part result = new Part();
		result.setFilename(content.getFilename());
		result.setLocation(content.getTransport().getLocation());
		result.setSize(content.getSize());
		result.setChecksum(content.getChecksum());
		return result;
	}

}
