/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.job;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import nl.logius.digikoppeling.gb._2017._04.ChecksumType;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content.Transport;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content.Transport.Part;
import nl.ordina.digikoppeling.gb.model.send.Transaction;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Lifetime;
import nl.logius.digikoppeling.gb._2017._04.Location;
import nl.logius.digikoppeling.gb._2017._04.UrlType;

import org.apache.commons.codec.digest.DigestUtils;

public class DataReferenceRequestBuilder
{
	private Transaction transaction;
	private File file;
	private List<String> files;

	public DataReferenceRequestBuilder(Transaction transaction, File file, List<String> files)
	{
		this.transaction = transaction;
		this.file = file;
		this.files = files;
	}

	public DataReferenceRequest build() throws IOException
	{
		DataReferenceRequest result = new DataReferenceRequest();
		result.setLifetime(new Lifetime());
		result.setContent(createContent());
		return result;
	}
	
	private Content createContent() throws IOException
	{
		Content result = new Content();
		result.setFilename(file.getName());
		result.setContentType(transaction.getContentType());
		result.setSize(BigInteger.valueOf(file.length()));
		result.setCompression(transaction.getCompression().getCompression());
		result.setChecksum(createChecksum(file));
		result.setTransport(createTransport());
		return result;
	}

	private Transport createTransport() throws IOException
	{
		Transport result = new Transport();
		if (files.size() > 0)
			for (String filename : files)
				result.getPart().add(createPart(filename));
		else
			result.setLocation(createReceiverLocation(file.getName()));
		return result;
	}

	private Part createPart(String filename) throws IOException
	{
		Part result = new Part();
		File file = new File(filename);
		result.setFilename(file.getName());
		result.setLocation(createReceiverLocation(file.getName()));
		result.setSize(BigInteger.valueOf(file.length()));
		result.setChecksum(createChecksum(file));
		return result;
	}

	private Location createReceiverLocation(String filename)
	{
		Location result = new Location();
		result.setReceiverUrl(createUrlType(transaction.getUrl() + filename));
		return result;
	}

	private UrlType createUrlType(String url)
	{
		UrlType result = new UrlType();
		result.setValue(url);
		result.setType(result.getType());
		return result;
	}

	private ChecksumType createChecksum(File file) throws IOException
	{
		ChecksumType result = new ChecksumType();
		result.setType("MD5");
		result.setValue(DigestUtils.md5Hex(new FileInputStream(file)));
		return result;
	}

}
