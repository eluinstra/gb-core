/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.client;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.FactoryBean;

public class GBProxyFactory extends GBProxy implements FactoryBean<GBProxy>
{
	@Override
	public GBProxy getObject() throws Exception
	{
		if (StringUtils.isNotBlank(getHost()))
			return new GBProxy(getHost(),getPort(),getUsername(),getPassword(),getNonProxyHosts());
		else
			return null;
	}

	@Override
	public Class<?> getObjectType()
	{
		return GBProxy.class;
	}

	@Override
	public boolean isSingleton()
	{
		return true;
	}

}
