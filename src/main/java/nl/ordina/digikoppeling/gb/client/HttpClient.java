/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.conn.SchemePortResolver;
import org.apache.http.conn.UnsupportedSchemeException;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.conn.SystemDefaultRoutePlanner;

import com.github.sardine.Sardine;
import com.github.sardine.impl.SardineException;
import com.github.sardine.impl.SardineImpl;

public class HttpClient
{
	class GBProxySelector extends ProxySelector
	{
		@Override
		public List<Proxy> select(URI uri)
		{
			return Arrays.asList(new Proxy(Type.HTTP,new InetSocketAddress(proxy.getHost(),proxy.getPort())));
		}
		
		@Override
		public void connectFailed(URI uri, SocketAddress sa, IOException ioe)
		{
		}
	}

	class GBAuthenticator extends Authenticator
	{
		@Override
		protected PasswordAuthentication getPasswordAuthentication()
		{
			return new PasswordAuthentication(proxy.getUsername(),proxy.getPassword().toCharArray());
		}
	}

	private SSLFactoryManager sslFactoryManager;
	private String[] enabledProtocols;
	private String[] enabledCipherSuites;
	private boolean verifyHostnames;
	private GBProxy proxy;

	public HttpClient(SSLFactoryManager sslFactoryManager, String[] enabledProtocols, String[] enabledCipherSuites, boolean verifyHostnames, GBProxy proxy)
	{
		this.sslFactoryManager = sslFactoryManager;
		this.enabledProtocols = enabledProtocols;
		this.enabledCipherSuites = enabledCipherSuites;
		this.verifyHostnames = verifyHostnames;
		this.proxy = proxy;
		if (proxy != null)
		{
			//if (proxy.useProxy(url))
				//ProxySelector.setDefault(new GBProxySelector());
			if (proxy.useProxyAuthorization())
				Authenticator.setDefault(new GBAuthenticator());
		}
	}

	public void sendMessage(String filename, final String url) throws IOException
	{
		try
		{
			Sardine sardine = new SardineImpl()
			{
				@Override
				protected ConnectionSocketFactory createDefaultSecureSocketFactory()
				{
					return new SSLConnectionSocketFactory(sslFactoryManager.getSslContext(),enabledProtocols.length == 0 ? null : enabledProtocols,enabledCipherSuites.length == 0 ? null : enabledCipherSuites,verifyHostnames ? new DefaultHostnameVerifier() : NoopHostnameVerifier.INSTANCE);
				}
				
				@Override
				protected HttpRoutePlanner createDefaultRoutePlanner(SchemePortResolver resolver, ProxySelector selector)
				{
					try
					{
						if (proxy == null || !proxy.useProxy(url))
							return super.createDefaultRoutePlanner(resolver,selector);
						else
							return new SystemDefaultRoutePlanner(new SchemePortResolver()
							{
								@Override
								public int resolve(HttpHost host) throws UnsupportedSchemeException
								{
									return host.getPort();
								}
							},new GBProxySelector()
							);
					}
					catch (MalformedURLException e)
					{
						throw new RuntimeException(e);
					}
				}
			};
			FileInputStream fis = new FileInputStream(filename);
			sardine.put(url,fis);
		}
		catch(SardineException e)
		{
			throw new IOException(e);
		}
	}

}