/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.dao;

import java.util.List;

import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content.Transport.Part;
import nl.ordina.digikoppeling.gb.model.send.Compression;
import nl.ordina.digikoppeling.gb.model.send.Status;
import nl.ordina.digikoppeling.gb.model.send.StatusInfo;
import nl.ordina.digikoppeling.gb.model.send.Transaction;

public interface GBDAO
{
	void executeTransaction(DAOTransactionCallback callback) throws DAOException;

	Transaction getTransaction(String transactionId) throws DAOException;

	Status getStatus(String transactionId) throws DAOException;

	StatusInfo getStatusInfo(String transactionId) throws DAOException;

	List<String> getTransactionIds(Status status) throws DAOException;

	List<String> getFilenames(String transactionId, Status status) throws DAOException;

	void insertTransaction(String transactionId, String filename, String contentType, String url, long maxFileSize, Compression compression, Status status) throws DAOException;

	void updateTransaction(String transactionId, Status status) throws DAOException;

	void updateTransaction(String transactionId, Status status, String message) throws DAOException;

	void updateTransaction(String transactionId, DataReferenceRequest dataReference, List<Part> parts, Status status) throws DAOException;

	void updateFile(String transactionId, String filename, Status status) throws DAOException;

	void updateFiles(String transactionId, Status failed, Status unprocessed) throws DAOException;

	void deleteTransaction(String transactionId) throws DAOException;
}
