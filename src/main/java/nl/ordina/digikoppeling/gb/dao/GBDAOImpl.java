/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest.Content.Transport.Part;
import nl.ordina.digikoppeling.gb.JAXBParser;
import nl.ordina.digikoppeling.gb.model.send.Compression;
import nl.ordina.digikoppeling.gb.model.send.Status;
import nl.ordina.digikoppeling.gb.model.send.StatusInfo;
import nl.ordina.digikoppeling.gb.model.send.Transaction;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

public class GBDAOImpl implements GBDAO
{
	protected TransactionTemplate transactionTemplate;
	protected JdbcTemplate jdbcTemplate;
	
	public GBDAOImpl(TransactionTemplate transactionTemplate, JdbcTemplate jdbcTemplate)
	{
		this.transactionTemplate = transactionTemplate;
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void executeTransaction(final DAOTransactionCallback callback) throws DAOException
	{
		try
		{
			transactionTemplate.execute(
				new TransactionCallbackWithoutResult()
				{

					@Override
					protected void doInTransactionWithoutResult(TransactionStatus transactionStatus)
					{
						callback.doInTransaction();
					}
				}
			);
		}
		catch (DataAccessException | TransactionException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public Transaction getTransaction(String transactionId) throws DAOException
	{
		try
		{
			return jdbcTemplate.queryForObject(
				"select id, filename, content_type, url, max_file_size, compression, data_reference, status from gb_transaction where id = ?",
				new RowMapper<Transaction>()
				{
					@Override
					public Transaction mapRow(ResultSet rs, int index) throws SQLException
					{
						try
						{
							return new Transaction(rs.getString("id"),rs.getString("filename"),rs.getString("content_type"),rs.getString("url"),rs.getLong("max_file_size"),Compression.values()[rs.getInt("compression")],JAXBParser.getInstance(DataReferenceRequest.class).handle(rs.getString("data_reference"),DataReferenceRequest.class),Status.values()[rs.getInt("status")]);
						}
						catch (JAXBException e)
						{
							throw new SQLException(e);
						}
					}
				},
				transactionId
			);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public Status getStatus(String transactionId) throws DAOException
	{
		try
		{
			return Status.values()[jdbcTemplate.queryForObject("select status from gb_transaction where id = ?",Integer.class,transactionId)];
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public StatusInfo getStatusInfo(String transactionId) throws DAOException
	{
		try
		{
			return jdbcTemplate.queryForObject(
				"select status, status_time, status_message from gb_transaction where id = ?",
				new RowMapper<StatusInfo>()
				{
					@Override
					public StatusInfo mapRow(ResultSet rs, int index) throws SQLException
					{
						return new StatusInfo(Status.values()[rs.getInt("status")],rs.getTimestamp("status_time"),rs.getString("status_message"));
					}
				},
				transactionId
			);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public List<String> getTransactionIds(Status status) throws DAOException
	{
		try
		{
			return jdbcTemplate.queryForList(
				"select id from gb_transaction where (? is null or status = ?) order by status_time desc",
				String.class,
				status == null ? null : status.ordinal(),
				status == null ? null : status.ordinal()
			);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public List<String> getFilenames(String transactionId, Status status) throws DAOException
	{
		try
		{
			return jdbcTemplate.queryForList(
				"select filename from gb_file where gb_transaction_id = ? and status = ?",
				String.class,
				transactionId,
				status.ordinal()
			);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void insertTransaction(String transactionId, String filename, String contentType, String url, long maxFileSize, Compression compression, Status status)
	{
		try
		{
			Date now = new Date();
			jdbcTemplate.update
			(
				"insert into gb_transaction (id,timestamp,filename,content_type,url,max_file_size,compression,status,status_time) values (?,?,?,?,?,?,?,?,?)",
				transactionId,
				now,
				filename,
				contentType,
				url,
				maxFileSize,
				compression.ordinal(),
				status.ordinal(),
				now
			);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void updateTransaction(String transactionId, Status status) throws DAOException
	{
		try
		{
			updateTransaction(transactionId,status,null);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void updateTransaction(String transactionId, Status status, String message) throws DAOException
	{
		try
		{
			jdbcTemplate.update("update gb_transaction set status = ?, status_time = ?, status_message = ? where id = ?",status.ordinal(),new Date(),message,transactionId);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void updateTransaction(final String transactionId, final DataReferenceRequest dataReference, final List<Part> parts, final Status status) throws DAOException
	{
		try
		{
			transactionTemplate.execute(
					new TransactionCallbackWithoutResult()
					{
						@Override
						public void doInTransactionWithoutResult(TransactionStatus arg0)
						{
							try
							{
								jdbcTemplate.update
								(
									"update gb_transaction set data_reference = ?, status = ?, status_time = ? where id = ?",
									JAXBParser.getInstance(DataReferenceRequest.class).handle(createJAXBElement(dataReference)),
									status.ordinal(),
									new Date(),
									transactionId
								);
								insertFiles(transactionId,dataReference.getContent());
							}
							catch (JAXBException e)
							{
								throw new RuntimeException(e);
							}
						}
					}
			);
		}
		catch (Exception e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void updateFile(String transactionId, String filename, Status status) throws DAOException
	{
		try
		{
			jdbcTemplate.update("update gb_file set status = ? where gb_transaction_id = ? and filename = ?",status.ordinal(),transactionId,filename);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}
	
	@Override
	public void updateFiles(String transactionId, Status oldStatus, Status newStatus) throws DAOException
	{
		try
		{
			jdbcTemplate.update("update gb_file set status = ? where gb_transaction_id = ? and status = ?",newStatus.ordinal(),transactionId,oldStatus.ordinal());
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void deleteTransaction(final String transactionId)
	{
		try
		{
			transactionTemplate.execute(
				new TransactionCallbackWithoutResult()
				{
					@Override
					public void doInTransactionWithoutResult(TransactionStatus arg0)
					{
						jdbcTemplate.update("delete from gb_file where gb_transaction_id = ?",transactionId);
						jdbcTemplate.update("delete from gb_transaction where id = ?",transactionId);
					}
				}
			);
		}
		catch (TransactionException e)
		{
			throw new DAOException(e);
		}
	}

	private void insertFiles(final String transactionId, Content content)
	{
		final List<Part> parts = content.getTransport().getPart();
		if (parts.size() > 0)
		{
			jdbcTemplate.batchUpdate
			(
				"insert into gb_file (gb_transaction_id,filename,status) values (?,?,?)",
				new BatchPreparedStatementSetter()
				{
					@Override
					public void setValues(PreparedStatement ps, int row) throws SQLException
					{
						ps.setString(1,transactionId);
						ps.setString(2,parts.get(row).getFilename());
						ps.setInt(3,Status.READY_TO_SEND.ordinal());
					}
					
					@Override
					public int getBatchSize()
					{
						return parts.size();
					}
				}
			);
		}
		else
		{
			jdbcTemplate.update
			(
				"insert into gb_file (gb_transaction_id,filename,status) values (?,?,?)",
				transactionId,
				content.getFilename(),
				Status.READY_TO_SEND.ordinal()
			);
		}
	}

	private JAXBElement<DataReferenceRequest> createJAXBElement(DataReferenceRequest dataReferenceRequest)
	{
		return new JAXBElement<DataReferenceRequest>(new QName("http://www.logius.nl/digikoppeling/gb/2017/04","data-reference-request"),DataReferenceRequest.class,null,dataReferenceRequest);
	}

}
