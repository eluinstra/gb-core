/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.model.send;

import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest;

public class Transaction
{
	private String id;
	private String filename;
	private String contentType;
	private String url;
	private long maxFileSize;
	private Compression compression;
	private DataReferenceRequest dataReference;
	private Status status;

	public Transaction(String id, String filename, String contentType, String url, long maxFileSize, Compression compression, Status status)
	{
		this(id,filename,contentType,url,maxFileSize,compression,null,status);
	}

	public Transaction(String id, String filename, String contentType, String url, long maxFileSize, Compression compression, DataReferenceRequest dataReference, Status status)
	{
		this.id = id;
		this.filename = filename;
		this.contentType = contentType;
		this.url = url;
		this.maxFileSize = maxFileSize;
		this.compression = compression;
		this.dataReference = dataReference;
		this.status = status;
	}

	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getFilename()
	{
		return filename;
	}
	public void setFilename(String filename)
	{
		this.filename = filename;
	}
	public String getContentType()
	{
		return contentType;
	}
	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}
	public String getUrl()
	{
		return url;
	}
	public void setUrl(String url)
	{
		this.url = url;
	}
	public long getMaxFileSize()
	{
		return maxFileSize;
	}
	public void setMaxFileSize(long maxFileSize)
	{
		this.maxFileSize = maxFileSize;
	}
	public Compression getCompression()
	{
		return compression;
	}
	public void setCompression(Compression compression)
	{
		this.compression = compression;
	}
	public DataReferenceRequest getDataReference()
	{
		return dataReference;
	}
	public void setDataReference(DataReferenceRequest dataReference)
	{
		this.dataReference = dataReference;
	}
	public Status getStatus()
	{
		return status;
	}
	public void setStatus(Status status)
	{
		this.status = status;
	}
}
