/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.model.send;

public enum Compression
{
	NONE(nl.logius.digikoppeling.gb._2017._04.Compression.NONE), ZIP4J(nl.logius.digikoppeling.gb._2017._04.Compression.ZIP_4_J);
	
	private nl.logius.digikoppeling.gb._2017._04.Compression compression;

	private Compression(nl.logius.digikoppeling.gb._2017._04.Compression compression)
	{
		this.compression = compression;
	}

	public static Compression getDefault(Compression compression)
	{
		return compression == null ? Compression.NONE : compression;
	}

	public nl.logius.digikoppeling.gb._2017._04.Compression getCompression()
	{
		return compression;
	}
}
