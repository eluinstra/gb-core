/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

public class FileSplitter
{
	private String directory;
	private File file;
	private long maxSize;
	private List<String> splitFiles = new ArrayList<String>();

	public FileSplitter(String directory, File file, long maxSize)
	{
		this.directory = directory;
		this.file = file;
		this.maxSize = maxSize;
	}

	public void split() throws IOException
	{
		try (FileInputStream fis = new FileInputStream(file))
		{
			String fileName = file.getName();
			List<String> filePartNames = new ArrayList<String>();
			int i = 0;
			while (true)
			{
				String filePartName = String.format("%s.%03d",fileName,i++);
				File newFile = new File(directory,filePartName);
				try (FileOutputStream fos = new FileOutputStream(newFile))
				{
					if (maxSize > 0 ? IOUtils.copyLarge(fis,fos,0,maxSize) > 0 : IOUtils.copyLarge(fis,fos) > 0)
						filePartNames.add(newFile.getPath());
					else
					{
						newFile.delete();
						break;
					}
				}
			}
			splitFiles.clear();
			splitFiles.addAll(filePartNames);
		}
	}

	public List<String> getSplitFiles()
	{
		return splitFiles;
	}

}
