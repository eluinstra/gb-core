/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceResponse;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceResponse.Content;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceResponse.Content.Transport.Part;
import nl.ordina.digikoppeling.gb.Utils;
import nl.ordina.digikoppeling.gb.dao.DAOException;
import nl.ordina.digikoppeling.gb.dao.DAOTransactionCallback;
import nl.ordina.digikoppeling.gb.dao.GBDAO;
import nl.ordina.digikoppeling.gb.model.send.Compression;
import nl.ordina.digikoppeling.gb.model.send.Status;
import nl.ordina.digikoppeling.gb.model.send.StatusInfo;
import nl.ordina.digikoppeling.gb.model.send.Transaction;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GBServiceImpl implements GBService
{
	protected transient Log logger = LogFactory.getLog(getClass());
	private GBDAO gbDAO;
	private String workingDir;

	
	public GBServiceImpl(GBDAO gbDAO, String workingDir)
	{
		this.gbDAO = gbDAO;
		this.workingDir = Utils.correctUri(workingDir);
	}

	@Override
	public String sendFile(String filename, String contentType, String url, long maxSize, Compression compression) throws GBServiceException
	{
		logger.debug("sendFile " + filename);
		String transactionId = UUID.randomUUID().toString();
		logger.info("Creating transaction " + transactionId);
		String directory = workingDir + transactionId;
		if (!new File(directory).mkdir())
			throw new GBServiceException("Directory " + directory + " could not be created!");
		compression = Compression.getDefault(compression);
		gbDAO.insertTransaction(transactionId,filename,contentType,url,maxSize,compression,Status.PENDING);
		return transactionId;
	}

	@Override
	public DataReferenceRequest getDataReferenceRequest(String transactionId) throws GBServiceException
	{
		logger.debug("getDataReferenceRequest " + transactionId);
		try
		{
			Transaction transaction = gbDAO.getTransaction(transactionId);
			return transaction.getDataReference();
		}
		catch (DAOException e)
		{
			throw new GBServiceException(e);
		}
	}

	@Override
	public StatusInfo getStatus(String transactionId) throws GBServiceException
	{
		logger.debug("getStatus " + transactionId);
		try
		{
			return gbDAO.getStatusInfo(transactionId); 
		}
		catch (DAOException e)
		{
			throw new GBServiceException(e);
		}
	}

	@Override
	public List<String> getTransactionIds(Status status) throws GBServiceException
	{
		if (status == null)
			logger.debug("getTransationIds all");
		else
			logger.debug("getTransationIds " + status);
		return gbDAO.getTransactionIds(status);
	}

	@Override
	public void resumeTransaction(final String transactionId) throws GBServiceException
	{
		logger.debug("resumeTransaction " + transactionId);
		try
		{
			Transaction transaction = gbDAO.getTransaction(transactionId);
			if (Status.FAILED.equals(transaction.getStatus()))
			{
				gbDAO.executeTransaction(new DAOTransactionCallback()
				{
					@Override
					public void doInTransaction() throws DAOException
					{
						gbDAO.updateTransaction(transactionId,Status.READY_TO_SEND);
						gbDAO.updateFiles(transactionId,Status.FAILED,Status.READY_TO_SEND);
					}
				});
			}
			else
				throw new GBServiceException("Unable to resend transaction " + transactionId);
		}
		catch (DAOException e)
		{
			throw new GBServiceException(e);
		}
	}

	@Override
	public void resendTransaction(String transactionId, DataReferenceResponse dataReferenceResponse) throws GBServiceException
	{
		logger.debug("resendTransaction " + transactionId);
		try
		{
			Content content = dataReferenceResponse.getContent();
			if (nl.logius.digikoppeling.gb._2017._04.Status.ERROR.equals(content.getStatus()))
			{
				List<Part> parts = content.getTransport().getPart();
				if (parts.size() > 0)
				{
					for (Part part : parts)
						if (nl.logius.digikoppeling.gb._2017._04.Status.ERROR.equals(part.getStatus()))
							gbDAO.updateFile(transactionId,part.getFilename(),Status.READY_TO_SEND);
				}
				else
					gbDAO.updateFile(transactionId,content.getFilename(),Status.READY_TO_SEND);
				gbDAO.updateTransaction(transactionId,Status.READY_TO_SEND);
			}
		}
		catch (Exception e)
		{
			throw new GBServiceException(e);
		}
	}

	@Override
	public void deleteTransaction(String transactionId) throws GBServiceException
	{
		logger.debug("deleteTransaction " + transactionId);
		try
		{
			String directory = workingDir + transactionId;
			FileUtils.deleteDirectory(new File(directory));
			gbDAO.deleteTransaction(transactionId);
		}
		catch (DAOException | IOException e)
		{
			throw new GBServiceException(e);
		}
	}

}
