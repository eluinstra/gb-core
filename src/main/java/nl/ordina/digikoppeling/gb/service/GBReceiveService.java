/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.service;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import nl.logius.digikoppeling.gb._2010._10.DataReference;
import nl.ordina.digikoppeling.gb.model.receive.Status;
import nl.ordina.digikoppeling.gb.model.receive.StatusInfo;

@WebService(targetNamespace="http://www.ordina.nl/digikoppeling/gb/receive/3.0")
public interface GBReceiveService
{
  @WebResult(name="dataReference")
	DataReference createDataReference(
			@WebParam(name="filename") @XmlElement(required=true) String filename,
			@WebParam(name="contentType") @XmlElement(required=true) String contentType,
			@WebParam(name="url") @XmlElement(required=true) String url) throws GBServiceException;

	@WebResult(name="transactionId")
	String receiveFile(@WebParam(name="dataReference") @XmlElement(required=true) DataReference dataReference) throws GBServiceException;

  @WebResult(name="statusInfo")
  StatusInfo getStatus(@WebParam(name="transactionId") @XmlElement(required=true) String transactionId) throws GBServiceException;

  @WebResult(name="transactionId")
  List<String> getTransactionIds(@WebParam(name="status") Status status) throws GBServiceException;

  void resumeTransaction(@WebParam(name="transactionId") @XmlElement(required=true) String transactionId) throws GBServiceException;

	void deleteTransaction(@WebParam(name="transactionId") @XmlElement(required=true) String transactionId) throws GBServiceException;
}
